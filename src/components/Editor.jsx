import React, {useEffect} from "react";
import ReactMde from "react-mde";
import Showdown from "showdown";
import {useUpdateNote} from "../services/NotesQueries/useUpdateNote.js";
import {useNote} from "../services/NotesQueries/useNote.js";
import {useNotes} from "../services/NotesQueries/useNotes.js";
import {redirect} from "react-router-dom";

export default function Editor() {
    const {updateNote, isEditing} = useUpdateNote()
    const {notes} = useNotes();

    let {note} = useNote();

    if (!note) {
        note = notes[0]
    }

    const [selectedTab, setSelectedTab] = React.useState("write");
    const [text, setText] = React.useState(note.body)

    useEffect(() => {
        setText(() => note.body)
    }, [note]);

    const converter = new Showdown.Converter({
        tables: true,
        simplifiedAutoLink: true,
        strikethrough: true,
        tasklists: true,
    });

    function handleSave() {
        updateNote({text: text, id: note.id})
    }

    return (
        <section className="pane editor">
            <ReactMde
                value={text}
                onChange={setText}
                selectedTab={selectedTab}
                onTabChange={setSelectedTab}
                generateMarkdownPreview={(markdown) =>
                    Promise.resolve(converter.makeHtml(markdown))
                }
                minEditorHeight={80}
                heightUnits="vh"
            />
            <button
                onClick={handleSave}
                className="button-save"
            >
                Save
            </button>
        </section>
    );
}
