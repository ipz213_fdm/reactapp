import {useDeleteNote} from "../services/NotesQueries/useDeleteNote.js";
import {useNavigate} from "react-router-dom";

const Note = ({note}) => {
    const {deleteNote, isDeleting} = useDeleteNote()
    const currentNote = {}
    const navigate = useNavigate()

    return (
        <div key={note.id}>
            <div
                className={`title ${
                    note.id === currentNote.id ? "selected-note" : ""
                }`}
                onClick={() => navigate(`/notes/${note.id}`)}
            >
                <h4 className="text-snippet">{note.body.split("\n")[0]}</h4>
                <button
                    className="delete-btn"
                    onClick={() => deleteNote(note.id)}
                >
                    <i className="gg-trash trash-icon"></i>
                </button>
            </div>
        </div>
    );
};

export default Note;