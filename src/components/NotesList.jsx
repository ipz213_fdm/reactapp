import React from 'react';
import Note from "./Note";
import {useNotes} from "../services/NotesQueries/useNotes.js";

const NotesList = () => {
  const {notes, isLoading} = useNotes()

  if (isLoading)
    return (
      <span>Loading...</span>
    );

  return (
    <>
      {notes.map((note, index) => (
        <Note note={note} key={index}/>
      ))}
    </>
  );
};

export default NotesList;