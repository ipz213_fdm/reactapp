import React from "react"
import NotesList from "./NotesList";
import {useAddNote} from "../services/NotesQueries/useAddNote.js";

export default function Sidebar() {
  const {createNewNote, isLoading} = useAddNote()

  return (
    <section className="pane sidebar">
      <div className="sidebar--header">
        <h3>Notes</h3>
        <button
          className="new-note"
          onClick={createNewNote}
        >+</button>
      </div>
      <NotesList/>
    </section>
  )
}
