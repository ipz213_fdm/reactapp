import { useMutation, useQueryClient } from "@tanstack/react-query";
import toast from "react-hot-toast";
import {updateNote as updateNoteApi} from "../ApiNotes.js";

export function useUpdateNote() {
  const queryClient = useQueryClient();

  const { mutate: updateNote, isLoading: isEditing } = useMutation({
    mutationFn: ({ text, id }) => updateNoteApi(text, id),
    onSuccess: () => {
      toast.success("Note successfully edited");
      queryClient.invalidateQueries({
        queryKey: ["notes"],
      });
    },
    onError: (err) => toast.error(err.message),
  });

  return { isEditing, updateNote };
}
