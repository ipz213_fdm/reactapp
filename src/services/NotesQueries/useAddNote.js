import {useMutation, useQueryClient} from "@tanstack/react-query";
import toast from "react-hot-toast";
import {createNewNote as createNewNoteApi} from "../ApiNotes.js";

export function useAddNote() {
  const queryClient = useQueryClient();

  const {mutate: createNewNote, isLoading} = useMutation({
    mutationFn: createNewNoteApi,
    onSuccess: () => {
      toast.success("New note successfully created. Congratulations!")
      queryClient.invalidateQueries({
        queryKey: ["notes"],
      })
    },
    onError: (err) => toast.error(err.message)
  })

  return {createNewNote, isLoading};
}