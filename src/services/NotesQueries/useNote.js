import {useQuery} from "@tanstack/react-query";
import {getNote} from "../ApiNotes.js";
import {useParams} from "react-router-dom";

export function useNote() {
  const {noteId} = useParams()

  const {isLoading, data: note, error} = useQuery({
    queryKey: ["notes", noteId],
    queryFn: () => getNote(noteId),
    retry: false
  })

  return {note, isLoading, error};
}