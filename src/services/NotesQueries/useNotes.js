import {useQuery} from "@tanstack/react-query";
import {getNote, getNotes} from "../ApiNotes.js";

export function useNotes() {
  const {isLoading, data: notes, error} = useQuery({
    queryKey: ["notes"],
    queryFn: getNotes,
    retry: false
  })

  return {notes, isLoading, error};
}