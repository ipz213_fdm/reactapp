import {nanoid} from "nanoid";

const BASE_URL = "http://localhost:8000"

export async function createNewNote() {
  const newNote = {
    id: nanoid(),
    body: "# Type your markdown note's title here"
  }

  let data;
  try {
    const res = await fetch(`${BASE_URL}/notes`, {
      method: "POST",
      body: JSON.stringify(newNote),
      headers: {
        "Content-Type": "application/json",
      },
    });
    data = await res.json();
  } catch (err) {
    throw new Error("There is an error adding the note.")
  }

  return data;
}

export async function getNotes() {
  let data;

  try {
    const res = await fetch(`${BASE_URL}/notes`);
    data = await res.json();
  } catch (e) {
    throw new Error("There is an error loading data")
  }

  if (!data)
    return null;

  return data;
}

export async function getNote(id) {
  let data;

  try {
    const res = await fetch(`${BASE_URL}/notes`);
    data = await res.json();
    data = data.find(
        city => city.id === id
    );
  } catch (e) {
    throw new Error("Note could not be found.")
  }

  return data;
}

export async function updateNote(text, currentNoteId) {
  try {
    await fetch(`${BASE_URL}/notes/${currentNoteId}`, {
      method: "PATCH",
      body: JSON.stringify({body: text}),
      headers: {
        "Content-Type": "application/json",
      },
    });
  } catch (err) {
    throw new Error("There is an error updating note.")
  }
}

export async function deleteNote(id) {
  try {
    await fetch(`${BASE_URL}/notes/${id}`, {
      method: "DELETE",
    });
  } catch {
    throw new Error("There is an error deleting the note.")
  }
}