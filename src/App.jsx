import React from "react"
import {useNotes} from "./services/NotesQueries/useNotes.js";
import {BrowserRouter, Route, Routes} from "react-router-dom";
import HomePage from "../../../Desktop/notes/src/pages/HomePage.jsx";
import {Toaster} from "react-hot-toast";
import NotesPage from "../../../Desktop/notes/src/pages/NotesPage.jsx";
import PageNotFound from "../../../Desktop/notes/src/pages/PageNotFound.jsx";

export default function App() {
    const { notes, isLoading } = useNotes()
    if (isLoading) return <span>Loading...</span>

    return (
        <main>
            <BrowserRouter>
                <Routes>
                    <Route index element={<HomePage/>}/>
                    <Route path="notes/" element={<NotesPage/>}/>
                    <Route path="notes/:noteId" element={<NotesPage/>}/>
                    <Route path="*" element={<PageNotFound />} />
                </Routes>
            </BrowserRouter>
            <Toaster
                position="bottom-right"
                gutter={12}
                containerStyle={{ margin: "8px" }}
                toastOptions={{
                    success: { duration: 3000 },
                    error: { duration: 5000 },
                    style: {
                        fontSize: "16px",
                        maxWidth: "500px",
                        padding: "16px 24px",
                        backgroundColor: "var(--color-dark--0)",
                        color: "var(--color-light--3)",
                    },
                }}
            />
        </main>
    )
}
