import React from 'react';
import {useAddNote} from "../services/NotesQueries/useAddNote.js";
import {useNotes} from "../services/NotesQueries/useNotes.js";
import {redirect, useNavigate} from "react-router-dom";

const HomePage = () => {
  const {createNewNote} = useAddNote()
  const {notes} = useNotes()
  const navigate = useNavigate()

  if (notes.length !== 0)
    navigate(`/notes/${notes[0].id}`)

  return (
    <div className="no-notes">
      <h1>You have no notes</h1>
      <button
        className="first-note"
        onClick={createNewNote}
      >
        Create one now
      </button>
    </div>
  );
};

export default HomePage;