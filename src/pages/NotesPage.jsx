import React, {useEffect} from 'react';
import Sidebar from "/src/components/Sidebar.jsx";
import Editor from "/src/components/Editor.jsx";
import Split from "react-split";
import {useNotes} from "../services/NotesQueries/useNotes.js";
import {redirect, redirectDocument, useNavigate} from "react-router-dom";
import HomePage from "./HomePage.jsx";

const NotesPage = () => {
    const {notes} = useNotes()
    const navigate = useNavigate()

    if (notes.length === 0)
        return navigate('/')

    return (
        <>
            <Split
                sizes={[30, 70]}
                direction="horizontal"
                className="split"
            >
                <Sidebar/>
                <Editor/>
            </Split>
        </>
    );
};

export default NotesPage;